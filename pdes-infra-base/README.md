# pdes-infra-base

A service account need to be created to manage the environment with terraform.

In this project the account created were:

|Name Account|Environment|
|---|---|
|tf-service@pdes-production.iam.gserviceaccount.com|Production|
|tf-service@pdes-development.iam.gserviceaccount.com|Development|

```
## Example

## Create User
gcloud beta iam service-accounts create {accountname} --description "Service Account for terraform tasks in Development" --display-name  "{accountname}"

## Get Private Key
gcloud iam service-accounts keys create --iam-account tf-service@pdes-production.iam.gserviceaccount.com {filename}.json
```

Submodules

|Name|Description|
|---|---|
|bucket|Bucket management|
|cloudflare|Cloudflare management|
|||