variable "project" {
  description = "Name of the google cloud project to be used"
  default     = "pdes-production"
}
variable "main_region" {
  description = "Main region to mantain GCP resources"
  default     = "us-central1"
}
variable "main_zone" {
  description = "Main zone to deploy resources"
  default     = "us-central1-b"
}
variable "environment" {
  description = "Name for this environment"
  default     = "gamma"
}
variable "platform" {
  description = "This is the internal project name for this whole stack"
  default     = "pdes"
}
variable "internal_domain_zone" {
  default = "gamma-pdes-int"
}
variable "project_fqdn" {
  default = "mon5.com"
}
variable "cloudflare_email" {
  default = "eidast@gmail.com"
}
variable "subnets" {
  description = ""

  default = [
    {
      subnet_name           = "southcarolina"
      subnet_ip             = "10.55.0.0/16"
      subnet_region         = "us-east1"
      subnet_private_access = true
    },
    {
      subnet_name           = "iowa"
      subnet_ip             = "10.56.0.0/16"
      subnet_region         = "us-central1"
      subnet_private_access = true
    },
    {
      subnet_name           = "oregon"
      subnet_ip             = "10.57.0.0/16"
      subnet_region         = "us-west1"
      subnet_private_access = true
    }
  ]
}

variable "secundary_ip_ranges_pods" {
  description = ""

  default = [
    {
      range_name    = "southcarolina-pods"
      ip_cidr_range = "10.65.0.0/16"
    },
    {
      range_name    = "iowa-pods"
      ip_cidr_range = "10.66.0.0/16"
    },
    {
      range_name    = "oregon-pods"
      ip_cidr_range = "10.67.0.0/16"
    }
  ]
}

variable "secundary_ip_ranges_services" {
  description = ""

  default = [
    {
      range_name    = "southcarolina-services"
      ip_cidr_range = "10.75.0.0/16"
    },
    {
      range_name    = "iowa-services"
      ip_cidr_range = "10.76.0.0/16"
    },
    {
      range_name    = "oregon-services"
      ip_cidr_range = "10.77.0.0/16"
    }
  ]
}

variable "mt_bastion" {
  description = "Micro tier machine type for Google Computing"
  default     = "f1-micro"
}
variable "mt_jenkins_master" {
  description = "Micro tier machine type for Google Computing"
  default     = "n1-standard-1"
}
variable "mt_jenkins_worker" {
  description = "machine type for Google Computing"
  default     = "n1-standard-2"
}
variable "default_service_account" {
  description = "Default service account used by the project"
  default     = "831290669316-compute@developer.gserviceaccount.com"
}
variable "bastion_security_scope" {
  type    = "list"
  default = ["userinfo-email", "compute-ro", "storage-ro", "logging-write", "monitoring-write", "cloud-platform"]
}
variable "workers_security_scope" {
  type    = "list"
  default = ["userinfo-email", "compute-ro", "storage-rw", "logging-write", "monitoring-write", "cloud-platform"]
}
variable "bastion_image" {
  default = "bastion-pdes-1568436921"
}
variable "jenkins_image" {
  default = "jenkins-pdes-1568437081"
}
variable "jenkins_worker_image" {
  default = "jenkins-worker-pdes-1568443039"
}
variable "ssh_keys" {
  description = "Accepted SSH keys"

  default = [
    "eidast:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDF0cJz5fq5Ksl4a+il3yDUSjg/Skkfieo0MGNpBZIEt6GW4LqjebIyNLbsDpbMxP1+RVE/W7A0qaZ4z2+CyDICotXrlh4YxwoZPQQfjKXTC8pQCfE47byEcXkFaaPJ/T3mvwGD65d09q+XEq5Om/mklArOmSe66KUNYJ1dbViHG27kkBf9ATAPJvmMqPkQMkRUBmiitI3+hQoRTN7nV6l0SjlHp6dK1F2V+7IheK/m6VTDD4kgh+WuVhu+u53s9+yXOsqkb/JVedmF6OPNbc1SpNAlTYe6VOkTE5a+zzIt7Oh3lcTV3OhoNTxpw+ROwkYQS+5SZP+P+y92mjllTUZh eidast@phoenix",
  ]
}
variable "workers_ssh_keys" {
  default = "build:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPljILJk9k5oeIeWLaMDgMgs9jV9MUl80v0j2I8cZqtvW1yctU0uef+05rJl15E6chW48gWqd3iGm1nfz+VVFecDBBIwIaQq9V0SPesIeK3YfmKI7fsja6EInhEaf2Z7zffS52/LKru8W42CjwqEa+RIXahZ7l+u0CAcBPZOyqFvAXIK6GXpVuAzKU2mTIlrUTBmKp21JuKQmNeifXj57+nhSvOQ2GfxwWRejeiQVK9B3yjHS/+TkuyPV0xucNBhKAbcjesbf/QsJ4jnnGXcgYD3m/3/yDHSH+vO4h8gwlm0ilTb7DHLqf50yYY0Gtcp8+QLBvMZDyJiMfIlCJH/3l build@jenkins-worker"
}

variable "jenkins_security_scope" {
  type    = "list"
  default = ["userinfo-email", "compute-ro", "storage-rw", "logging-write", "monitoring-write"]
}
variable "jenkins_worker_security_scope" {
  type    = "list"
  default = ["userinfo-email", "compute-ro", "sql", "datastore", "storage-rw", "logging-write", "monitoring-write", "cloud-platform"]
}
locals {
  dns_records = [
    ["${var.project_fqdn}", "gamma-pdes-k8s-m-0", "34.102.171.178", "A", true, 0],
    ["${var.project_fqdn}", "timeoff", "gamma-pdes-k8s-m-0.${var.project_fqdn}", "CNAME", true, 0],
    ["${var.project_fqdn}", "gamma-pdes-je-m-0", "${google_compute_address.ext-je-m-0.address}", "A", true, 0],
    ["${var.project_fqdn}", "gamma-pdes-ba-s-0", "${google_compute_address.ext-ba-s-0.address}", "A", false, 0],
    ["${var.project_fqdn}", "gjenkins", "gamma-pdes-je-m-0.${var.project_fqdn}", "CNAME", true, 0],
    ["${var.project_fqdn}", "gbastion", "gamma-pdes-ba-s-0.${var.project_fqdn}", "CNAME", false, 0]
  ]
}