output "environment" {
  value = "${var.environment}"
}
output "project" {
  value = "${var.project}"
}
output "platform" {
  value = "${var.platform}"
}
output "main_region" {
  value = "${var.main_region}"
}
output "default_service_account" {
  value = "${var.default_service_account}"
}

output "main-network_name" {
  value = "${module.main_network.network_name}"
}
output "internal_domain_zone" {
  value = "${var.internal_domain_zone}"
}
output "main-network_self_link" {
  value       = "${module.main_network.network_self_link}"
  description = "The URI of the VPC being created"
}
output "main-network-subnets_self_links" {
  value       = "${module.main_network.subnets_self_links}"
  description = "The self-links of subnets being created"
}
output "main-network-subnets_regions" {
  value       = "${module.main_network.subnets_regions}"
  description = "The region where the subnets will be created"
}
output "main-network-subnets_secundary_ip_range" {
  value       = "${module.main_network.subnets_secundary_ip_range}"
  description = "The region where the subnets will be created"
}