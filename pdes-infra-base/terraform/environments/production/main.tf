# Production
provider "google" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${var.project}"
  region      = "${var.main_region}"
}
provider "google-beta" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${var.project}"
  region      = "${var.main_region}"
}
terraform {
  backend "gcs" {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-base/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

module "main_network" {
  source = "../../modules/resource/network"

  environment                  = "${var.environment}"
  platform                     = "${var.platform}"
  network_name                 = "network"
  subnets                      = "${var.subnets}"
  secundary_ip_ranges_services = "${var.secundary_ip_ranges_services}"
  secundary_ip_ranges_pods     = "${var.secundary_ip_ranges_pods}"
}
module "firewall" {
  source = "../../modules/resource/firewall_rules"

  environment  = "${var.environment}"
  network_name = "${module.main_network.network_name}"
  cidr_network = "10.0.0.0/8"
}
module "health_checks" {
  source = "../../modules/resource/health_checks"

  environment = "${var.environment}"
}
module "config_bucket" {
  source = "../../modules/resource/bucket"

  environment   = "${var.environment}"
  platform      = "${var.platform}"
  location      = "US"
  storage_class = "MULTI_REGIONAL"
  name          = "config"
}
module "backup_bucket" {
  source = "../../modules/resource/bucket"

  environment   = "${var.environment}"
  platform      = "${var.platform}"
  location      = "US"
  storage_class = "COLDLINE"
  name          = "backup"
}
module "cloudflare" {
  source = "../../modules/resource/cloudflare"

  cloudflare_email = "${var.cloudflare_email}"
  cloudflare_token = "${file("../../../../../../gl-secrets/cloudflare/cf-dns-management.token")}"
  domain           = "${var.project_fqdn}"
  dns_records      = "${local.dns_records}"
  min_tls_version  = "1.2"
}

module "bastion" {
  source = "../../modules/resource/single_instance"

  environment     = "${var.environment}"
  platform        = "${var.platform}"
  instance_name   = "ba-s-0"
  disk_image      = "${var.bastion_image}"
  zone            = "${var.main_zone}"
  machine_type    = "${var.mt_bastion}"
  subnetwork      = "${element(module.main_network.subnets_self_links, 1)}"
  network_address = "${google_compute_address.ext-ba-s-0.address}"
  security_scopes = "${var.bastion_security_scope}"
  service_account = "${var.default_service_account}"
  metadata = {
    ssh-keys           = "${join("\n", var.ssh_keys)}"
    startup-script-url = "gs://${module.config_bucket.name}/scripts/${var.environment}/startup/bastion.sh"
  }

  is_boot_disk_auto_delete = false
}

module "jenkins" {
  source = "../../modules/resource/single_instance"

  environment     = "${var.environment}"
  platform        = "${var.platform}"
  instance_name   = "m-0-je"
  disk_image      = "${var.jenkins_image}"
  data_disk_size  = 50
  zone            = "${var.main_zone}"
  machine_type    = "${var.mt_jenkins_master}"
  subnetwork      = "${element(module.main_network.subnets_self_links, 1)}"
  network_address = "${google_compute_address.ext-je-m-0.address}"
  security_scopes = "${var.jenkins_security_scope}"
  service_account = "${var.default_service_account}"
  metadata = {
    ssh-keys           = "${join("\n", var.ssh_keys)}"
    startup-script-url = "gs://${module.config_bucket.name}/scripts/${var.environment}/startup/jenkins.sh"
  }
  tags = ["app-server"]
}

module "template-build-slaves" {
  source = "../../modules/resource/instance_template_preemptible"

  environment     = "${var.environment}"
  prefix          = "pdes-n-je"
  tags            = ["jenkins"]
  description     = "Nodes for jenkins workers"
  machine_type    = "${var.mt_jenkins_worker}"
  disk_image      = "${var.jenkins_worker_image}"
  disk_size       = 20
  region          = "${var.main_region}"
  subnetwork      = "${element(module.main_network.subnets_self_links, 1)}"
  startup_script  = ""
  shutdown_script = ""
  ssh_keys        = "${var.workers_ssh_keys}"
  service_account = "${var.default_service_account}"
  scopes          = "${var.workers_security_scope}"
}

## Bastion
resource "google_compute_address" "ext-ba-s-0" {
  name = "${var.environment}-ext-ba-s-0-address"
}
resource "google_compute_address" "int-ba-s-0" {
  name         = "${var.environment}-int-ba-s-0-address"
  address_type = "INTERNAL"
  subnetwork   = "${element(module.main_network.subnets_self_links, 1)}"
}
## Jenkins
resource "google_compute_address" "ext-je-m-0" {
  name = "${var.environment}-ext-m-0-je-address"
}
resource "google_compute_address" "int-je-m-0" {
  name         = "${var.environment}-int-m-0-je-address"
  address_type = "INTERNAL"
  subnetwork   = "${element(module.main_network.subnets_self_links, 1)}"
}