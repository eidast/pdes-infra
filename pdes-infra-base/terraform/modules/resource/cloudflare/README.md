# cf-dns-records-tf-0.12

Author: Alexander Moreno Delgado
License: MIT License
Summary: This module manage Cloudflare using terraform 0.12

Short Name: cloudflare

## Provider Settings

|Variable|Description|
|--|--|
|domain|Domain Name|
|email|Username email|
|token|Authentication token|

## Zone Settings

|Variable|Description|
|--|--|
|tls_1_3|TLS 1.3, default: on|
|min_tls_version|Minimal version TLS, options: "1.0", "1.1", "1.2", "1.3", default: 1.3|
|https_rewrite|Automatic https rewrite|
|ssl|SSL Active, Options: "off", "flexible", "full", "strict", "origin_pull"|
|waf|Web Application Firewall, options: on, off, default: null (This feature require PRO)|

## DNS Records

|Variable|Description|
|--|--|
|domain|Domain Name|
|name|Record Name|
|value|Value for the record Software Component|
|type|Options: A, MX, CNAME, AAAA|
|proxied|If this record will be proxied by Cloudflare, Default: true, Options: true, false|
|priority|Numeric value to provide priority to a record. Default: 0|

## Example

- Define in locals if you want  
```json
locals {
  dns_records = [
    ["domain-example.com", "@", "{IP_ADDRESS}", "A", true, 0],
    ["domain-example.com", "server", "{IP_ADDRESS}", "A", true, 0],
    ["domain-example.com", "www", "@", "CNAME", true, 0],
    ["domain-example.com", "api", "server.domain-example.com", "CNAME", true, 0],
    ["domain-example.com", "admin", "server.domain-example.com", "CNAME", true, 0],
  ]
}
```

## DNS Additional information

- TTL by default is 1.

## References

https://www.terraform.io/docs/providers/cloudflare/r/record.html
https://www.terraform.io/docs/providers/cloudflare/r/zone_settings_override.html
