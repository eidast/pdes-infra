terraform {
  required_version = ">= 0.12"
}

provider "cloudflare" {
  email = var.cloudflare_email
  token = var.cloudflare_token
}

resource "cloudflare_zone_settings_override" "domain-settings" {
  name = var.domain

  settings {
    tls_1_3                  = var.tls_1_3
    min_tls_version          = var.min_tls_version
    automatic_https_rewrites = var.https_rewrites
    always_use_https         = var.always_use_https
    ssl                      = var.ssl
    waf                      = var.waf
  }
}

resource "cloudflare_record" "dns_normal_records" {
  count    = length(var.dns_records)
  domain   = element(var.dns_records[count.index], 0)
  name     = element(var.dns_records[count.index], 1)
  value    = element(var.dns_records[count.index], 2)
  type     = element(var.dns_records[count.index], 3)
  proxied  = element(var.dns_records[count.index], 4)
  priority = element(var.dns_records[count.index], 5)
  ttl      = "1"
}
