# Use:
# domain, name, value, type, proxied, priority,   
variable "dns_records" {
  type = list(tuple([string, string, string, string, bool, number]))
}
variable "cloudflare_email" {
  description = "Cloudflare user email"
}
variable "cloudflare_token" {
  description = "Cloudflare Access Token"
}
variable "domain" {
  description = "Cloudflare zone to config"
}

variable "tls_1_3" {
  description = "TLS 1.3 active"
  default     = "on"
}
variable "always_use_https" {
  description = "Always use SSL"
  default     = "on"
}
variable "https_rewrites" {
  description = "forward http to https"
  default     = "on"
}
variable "ssl" {
  description = "SSL active"
  default     = "strict"
}
variable "waf" {
  description = "WAF active"
  default     = null
}
variable "min_tls_version" {
  description = "Minimal TLS Version"
  default     = "1.3"
}