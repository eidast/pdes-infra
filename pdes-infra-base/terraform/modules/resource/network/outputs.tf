output "network_name" {
  value       = "${google_compute_network.network.name}"
  description = "The name of the VPC being created"
}

output "network_self_link" {
  value       = "${google_compute_network.network.self_link}"
  description = "The URI of the VPC being created"
}
output "db_internal_ip_alloc_self_link" {
  value       = "${google_compute_global_address.db_internal_ip_alloc.self_link}"
  description = "The URI of the VPC being created"
}

output "db_internal_ip_alloc_name" {
  value       = "${google_compute_global_address.db_internal_ip_alloc.name}"
  description = "The URI of the VPC being created"
}
output "subnets_self_links" {
  value       = "${google_compute_subnetwork.subnetwork.*.self_link}"
  description = "The self-links of subnets being created"
}
output "subnets_regions" {
  value       = "${google_compute_subnetwork.subnetwork.*.region}"
  description = "The region where the subnets will be created"
}

output "subnets_secundary_ip_range" {
  value = "${google_compute_subnetwork.subnetwork.*.secondary_ip_range}"
  description = "The secundary ip range for the subnets"
}
