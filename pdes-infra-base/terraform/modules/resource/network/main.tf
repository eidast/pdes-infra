resource "google_compute_network" "network" {
    name                    = "${var.environment}-${var.network_name}"
    auto_create_subnetworks = "false"

    description             = "built-by Terraform, module network"
}

resource "google_compute_subnetwork" "subnetwork" {
    count = "${length(var.subnets)}"

    name                        = "${var.environment}-${var.platform}-${lookup(var.subnets[count.index], "subnet_name")}"
    ip_cidr_range               = "${lookup(var.subnets[count.index], "subnet_ip")}"
    region                      = "${lookup(var.subnets[count.index], "subnet_region")}"
    private_ip_google_access    = "${lookup(var.subnets[count.index], "subnet_private_access", "false")}"
    enable_flow_logs            = "${lookup(var.subnets[count.index], "subnet_flow_logs", "false")}"
    network                     = "${google_compute_network.network.name}"

    secondary_ip_range = [
        {
            range_name    = "${var.environment}-${var.platform}-${lookup(var.secundary_ip_ranges_pods[count.index], "range_name")}"
            ip_cidr_range = "${lookup(var.secundary_ip_ranges_pods[count.index], "ip_cidr_range")}"
        },
        {
            range_name    = "${var.environment}-${var.platform}-${lookup(var.secundary_ip_ranges_services[count.index], "range_name")}"
            ip_cidr_range = "${lookup(var.secundary_ip_ranges_services[count.index], "ip_cidr_range")}"
        }
      ]
    description                 = "built-by Terraform, module network"
}
resource "google_compute_global_address" "db_internal_ip_alloc" {
    provider        = "google-beta"
    name            = "${var.environment}-${var.platform}-db-internal-alloc"
    purpose         = "VPC_PEERING"
    address_type    = "INTERNAL"
    prefix_length   = 16
    network         = "${google_compute_network.network.self_link}"

    depends_on = ["google_compute_network.network"]
}
resource "google_compute_router" "default_router" {
  name    = "${var.environment}-${var.platform}-${var.network_name}-router"
  region  = "${lookup(var.subnets[1], "subnet_region")}"
  network = "${google_compute_network.network.self_link}"
  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "simple-nat" {
  name                               = "${var.environment}-${var.platform}-${var.network_name}-nat"
  router                             = "${google_compute_router.default_router.name}"
  region                             = "${lookup(var.subnets[1], "subnet_region")}"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}