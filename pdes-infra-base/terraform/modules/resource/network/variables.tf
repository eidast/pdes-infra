variable "environment" {
    description = "Define which enviroment is deployed"
}
variable "platform" {
    description = "Platform or app to deploy"
}
variable "network_name" {
    description = "Network name for this"
}

variable "subnets" {
    description = "The list of subnets being created"
    type        = "list"
}

variable "secundary_ip_ranges_pods" {
    description = "The list of secundary subnet being created for pods"
    type        = "list"
}

variable "secundary_ip_ranges_services" {
    description = "The list of secundary subnet being created for services"
    type        = "list"
}