resource "google_compute_instance_template" "instance_template_preemptible" {
  name_prefix = "${var.environment}-${var.prefix}-"
  description = "This template is used to create template servers."
  region      = "${var.region}"

  tags = "${var.tags}"

  labels = {
    environment = "${var.environment}"
  }

  instance_description = "${var.description}"
  machine_type         = "${var.machine_type}"
  can_ip_forward       = false

  scheduling {
    preemptible         = true
    automatic_restart   = false
    on_host_maintenance = "TERMINATE"
  }

  // Create a new boot disk from an image
  disk {
    source_image = "${var.disk_image}"
    auto_delete  = true
    boot         = true
    disk_size_gb = "${var.disk_size}"
  }

  network_interface {
    subnetwork = "${var.subnetwork}"
    # No external ip Needed for workers
    #access_config {
      # Ephemeral
    #}
  }

  # metadata {
  #   startup-script      = "${var.startup_script}"
  #   shutdown-script     = "${var.shutdown_script}"
  #   sshKeys             = "${var.ssh_keys}"
  # }

  service_account {
    email   = "${var.service_account}"
    scopes  = "${var.scopes}"
  }

  lifecycle {
    create_before_destroy = true
  }
}