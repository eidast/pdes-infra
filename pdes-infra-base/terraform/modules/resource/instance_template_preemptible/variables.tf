variable "environment" {
    description = "Define which enviroment is deployed"
}

variable "prefix" {
    description = "Define which prefix will be used in the new instances"
}

variable "disk_image" {
    description = "Instance disk image"
}

variable "tags" {
    description = "List of tags to be used in this template"
    type        = "list"
}

variable "description" {
    description = "Description for this component"
}

variable "machine_type" {
    description = "What kind of machine I will use for this template"
}

variable "subnetwork" {
    description = "Subnetwork association with the instance"
}

variable "startup_script" {
    description = "Script to be executed at the moment of startup an instance"
}

variable "shutdown_script" {
    description = "Script to be executed at the moment of shutdown an instance"
}

variable "service_account" {
    description = "This is the service account associated with this instance template"
}

variable "ssh_keys" {
    description = "Provide a list of sshkeys to be used under those instances"
}

variable "scopes" {
    description = "Permissions allowed to those instances"
    type        = "list"
}

variable "region" {
    description = "In which region should created"
}

variable "disk_size" {
    description = "Space size disk for root disk in GB"
    default     = 10
}
