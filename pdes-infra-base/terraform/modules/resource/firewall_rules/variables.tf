variable "network_name" {
    description = "Network Name for firewall ruling"
}
variable "environment" {
    description = "Environment for firewall ruling"
}
variable "cidr_network" {
    description = "CIDR for network to allow internal traffic"
}