resource "google_compute_firewall" "default_common" {
  name          = "${var.environment}-ingress-default-common"
  network       = "${var.network_name}"
  description   = "${var.environment} - Allow SSH and ICMP traffic from anywhere"

    allow {
        protocol = "icmp"
    }

    allow {
        protocol = "tcp"
        ports    = ["22"]
    }
}

resource "google_compute_firewall" "allow_web_server" {
  name          = "${var.environment}-ingress-web-traffic"
  network       = "${var.network_name}"
  description   = "${var.environment} - Allow http and https traffic from anywhere"

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  target_tags = ["web-server"]
}

resource "google_compute_firewall" "allow_internal_traffic" {
  name          = "${var.environment}-allow-internal-traffic"
  network       = "${var.network_name}"
  description   = "${var.environment} - Allow internal traffic in whole VPC"

  allow {
    protocol = "tcp"
    ports    = ["0-65534"]
  }

  source_ranges = ["${var.cidr_network}"]
}

resource "google_compute_firewall" "allow_app_server" {
  name        = "${var.environment}-ingress-app-traffic"
  network     = "${var.network_name}"
  description = "${var.environment} - Allow 8080 and 8443 traffic from allowed networks"

  allow {
    protocol  = "tcp"
    ports     = ["8080", "8443"]
  }

  target_tags = ["app-server"]
}