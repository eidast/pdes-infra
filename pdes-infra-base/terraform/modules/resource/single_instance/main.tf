resource "google_compute_disk" "data_disk" {
  name = "${var.environment}-${var.platform}-${var.instance_name}-dd"
  type = "pd-standard"
  size = var.data_disk_size
  zone = var.zone
  labels = {
    environment = var.environment
    created_by  = "terraform"
    module      = "pdes-single_instance"
  }
}

resource "google_compute_instance" "instance" {
  count               = var.active
  name                = "${var.environment}-${var.platform}-${var.instance_name}"
  machine_type        = var.machine_type
  zone                = var.zone
  deletion_protection = true

  tags = var.tags

  labels = {
    built-by    = "terraform"
    repo        = "pdes-infra-base"
    environment = var.environment
  }

  boot_disk {
    auto_delete = var.is_boot_disk_auto_delete
    initialize_params {
      image = var.disk_image
    }
  }

  attached_disk {
    source = google_compute_disk.data_disk.name
  }

  allow_stopping_for_update = true

  network_interface {
    subnetwork = var.subnetwork

    access_config {
      nat_ip = var.network_address
    }
  }

  service_account {
    email  = var.service_account
    scopes = var.security_scopes
  }

  metadata = var.metadata

  depends_on = [google_compute_disk.data_disk]
}

