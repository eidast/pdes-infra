variable "instance_name" {
  description = "Name of the instance"
}
variable "environment" {
  description = "Enviroment"
}
variable "platform" {
  description = "Which component or platform will be deployef"
}

variable "machine_type" {
  description = "Machine kind"
}
variable "zone" {
  description = "Instance zone"
}
variable "disk_image" {
  description = "Instance disk image"
}
variable "subnetwork" {
  description = "Subnetwork association with the instance"
}
variable "network_address" {
  description = "IP Address for instance"
}
variable "security_scopes" {
  description = "List of roles defined for this instance"
  type        = list(string)
}
variable "service_account" {
  description = "Service Account for Instance"
}
variable "tags" {
  description = "network tags"
  default     = []
}
variable "metadata" {
  description = "Map of metadata values to pass to instances."
  type        = map(string)
  default     = {}
}
variable "is_boot_disk_auto_delete" {
  description = "Destroy disk at boot"
  default     = true
}
variable "data_disk_size" {
  description = "Size of the data disk"
  default     = 1
}
variable "active" {
  description = "Is the module active or no"
  default = 1
}
