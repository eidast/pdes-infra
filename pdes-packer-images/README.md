# pdes-packer-images

## Summary

This folder contains any packer configuration file required for build google images, to deploy our servers

## How to build a packer image

```
packer build {file.json}
```