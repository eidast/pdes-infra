- Tabs to open at the moment of the demo
  - Google Compute
  - Google Kubernetes Engine
  - Google SQL
  - Cloudflare
  - Jenkins
  - Stackdriver

- Demo to present (Optional)
  - Perform a failover with MySQL
  - Scale to 0 containers to shoot alarms
  - Deploy a new application
  - Deploy a new check
  - Create a read replica
  - Sent a commit