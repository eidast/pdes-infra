# Production

provider "google" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${data.terraform_remote_state.infra-base.outputs.project}"
  region      = "${data.terraform_remote_state.infra-base.outputs.main_region}"
}

provider "google-beta" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${data.terraform_remote_state.infra-base.outputs.project}"
  region      = "${data.terraform_remote_state.infra-base.outputs.main_region}"
}

terraform {
  backend "gcs" {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-gke/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

data "terraform_remote_state" "infra-base" {
  backend = "gcs"

  config = {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-base/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

module "gke" {
  source = "../../modules/resource/gke"

  environment                   = "${data.terraform_remote_state.infra-base.outputs.environment}"
  region                        = "${data.terraform_remote_state.infra-base.outputs.main_region}"
  platform                      = "${data.terraform_remote_state.infra-base.outputs.platform}"
  username                      = "admin_k8s"
  num_nodes                     = "${var.k8s_num_nodes}"
  prod_num_nodes                = "${var.k8s_prod_num_nodes}"
  extra_num_nodes               = "${var.k8s_extra_num_nodes}"
  network                       = "${data.terraform_remote_state.infra-base.outputs.main-network_self_link}"
  subnetwork                    = "${element(data.terraform_remote_state.infra-base.outputs.main-network-subnets_self_links, 1)}"
  cluster_secondary_range_name  = "gamma-pdes-iowa-pods"
  services_secondary_range_name = "gamma-pdes-iowa-services"
  secrets                       = "${local.secrets}"
  config_map                    = "${local.config_map}"
  certs                         = "${local.certs}"
  tls                           = "${data.terraform_remote_state.infra-base.outputs.platform}-tls"
  applications                  = "${var.deploy_apps}"
  is_preemptible                = true
}

locals {
  secrets = {
    "db.json"          = "${file("../../../../../../gl-secrets/app/${data.terraform_remote_state.infra-base.outputs.environment}/db.json")}"
    "credentials.json" = "${file("../../../../../../gl-secrets/db-service-production-550ba859e78f.json")}"
  }

  config_map = {
    NODE_ENV = "production"
    DB_USER  = "admin-33c7"
    DB_NAME  = "db_timeoff"
  }

  certs = [
    {
      name = "${data.terraform_remote_state.infra-base.outputs.platform}-tls"
      crt  = "${file("../../../../../../gl-secrets/cloudflare/cf-origin.crt")}"
      key  = "${file("../../../../../../gl-secrets/cloudflare/cf-origin.key")}"
    }
  ]
}