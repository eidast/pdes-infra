variable "k8s_num_nodes" {
  description = "Numbers of nodes for K8s"
  default     = 1
}

variable "k8s_prod_num_nodes" {
  description = "Numbers of nodes for K8s"
  default     = 0
}

variable "k8s_extra_num_nodes" {
  description = "Numbers of nodes for K8s"
  default     = 0
}

variable "deploy_apps" {
  description = "Applications to deploy"

  default = [
    {
      name         = "timeoff-application"
      image        = "gcr.io/pdes-production/timeoff-application:latest"
      min_replicas = 5
      max_replicas = 10
      port         = 80
      target_port  = 3000
      ingress_path = "/"
      ingress_host = "timeoff.mon5.com"
      probe_path   = "/login"
    },
  ]
}
