variable "environment" {
  description = "Define which enviroment is deployed"
}
variable "network" {
  description = "Network to be used in the GKE cluster"
}
variable "subnetwork" {
  description = "Sub network to be used in the GKE cluster"
}

variable "region" {
  description = "Region to be used in the master GKE cluster"
}

variable "username" {
  description = "K8s master username"
}

variable "num_nodes" {
  description = "Numbers of nodes for k8s preemtible"
}

variable "prod_num_nodes" {
  description = "Number of nodes for k8s not preemptible"
  default     = 0
}

variable "extra_num_nodes" {
  description = "Number of nodes for k8s are preemptible"
  default     = 0
}

variable "cluster_secondary_range_name" {
  description = "Name for the cluster CIDR"
}

variable "services_secondary_range_name" {
  description = "Name for the services CIDR"
}

variable "is_preemptible" {
  description = "This define if this node is preemptible or no"
  default     = false
}
variable "machine_type" {
  description = "This define which kind of machine type should use the node"
  default     = "n1-standard-1"
}
variable "machine_type_preemptible" {
  description = "This define which kind of machine type should use the node"
  default     = "n1-highcpu-2"
}
variable "machine_type_extra" {
  description = "This define which kind of machine type should use the node"
  default     = "n1-highcpu-4"
}
variable "applications" {
  description = "Applications to deploy"
  type        = "list"
}
variable "secrets" {
  description = "Secrets for this cluster"
  type        = "map"
}

variable "config_map" {
  description = "Config Map for this cluster"
  type        = "map"
}
variable "platform" {
}
variable "tls" {
}
variable "certs" {
  description = "TLS certs"
  type        = "list"
}
data "google_container_engine_versions" "k8s" {
  location       = "us-central1-b"
  version_prefix = "1.14."
}