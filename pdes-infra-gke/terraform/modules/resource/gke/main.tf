resource "random_id" "password" {
  byte_length = 8
}
resource "random_id" "cluster_id" {
  byte_length = 4
}
resource "google_container_cluster" "primary" {
  name               = "${var.environment}-${var.platform}-m-0-k8s"
  location           = "${var.region}"
  network            = "${var.network}"
  subnetwork         = "${var.subnetwork}"
  node_version       = "${data.google_container_engine_versions.k8s.latest_node_version}"
  min_master_version = "${data.google_container_engine_versions.k8s.latest_node_version}"
  description        = "built-by Terraform, module gke"

  remove_default_node_pool = true
  initial_node_count       = 1

  ip_allocation_policy {
    cluster_secondary_range_name  = "${var.cluster_secondary_range_name}"
    services_secondary_range_name = "${var.services_secondary_range_name}"
  }

  node_locations = [
    "us-central1-a",
    "us-central1-b",
    "us-central1-c",
    "us-central1-f"
  ]

  master_auth {
    username = "${var.username}"
    password = "${random_id.password.hex}"
  }
}


resource "google_container_node_pool" "nodes" {
  count = "${var.num_nodes}"

  name       = "${var.environment}-${var.platform}-n-${count.index}-k8s"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = "1"
  location   = "${var.region}"

  node_config {

    preemptible  = "${var.is_preemptible}"
    machine_type = "${var.machine_type_preemptible}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      built-by    = "terraform"
      module      = "gke"
      environment = "${var.environment}"
    }
  }
}

resource "google_container_node_pool" "extra" {
  count = "${var.extra_num_nodes}"

  name       = "${var.environment}-${var.platform}-e-${count.index}-k8s"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = "1"
  location   = "${var.region}"

  node_config {

    preemptible  = "true"
    machine_type = "${var.machine_type_extra}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      built-by    = "terraform"
      module      = "gke"
      environment = "${var.environment}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_container_node_pool" "production_nodes" {
  count = "${var.prod_num_nodes}"

  name       = "${var.environment}-${var.platform}-p-${count.index}-k8s"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = "1"
  location   = "${var.region}"

  node_config {

    preemptible  = false
    machine_type = "${var.machine_type}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      built-by    = "terraform"
      module      = "gke"
      environment = "${var.environment}"
    }
  }
}
provider "kubernetes" {
  host                   = "${google_container_cluster.primary.endpoint}"
  username               = "${var.username}"
  password               = "${random_id.password.hex}"
  client_certificate     = "${base64decode(google_container_cluster.primary.master_auth.0.client_certificate)}"
  client_key             = "${base64decode(google_container_cluster.primary.master_auth.0.client_key)}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
}
resource "kubernetes_secret" "secrets" {
  metadata {
    name = "${var.environment}-secrets-${random_id.cluster_id.hex}"
  }

  data = "${var.secrets}"
}
resource "kubernetes_config_map" "configs" {
  metadata {
    name = "${var.environment}-config-${random_id.cluster_id.hex}"
  }

  data = "${var.config_map}"
}
resource "kubernetes_secret" "tls" {
  count = "${length(var.certs)}"

  metadata {
    name = "${lookup(var.certs[count.index], "name")}"
  }

  data = {
    "tls.crt" = "${lookup(var.certs[count.index], "crt")}"
    "tls.key" = "${lookup(var.certs[count.index], "key")}"
  }

  type = "kubernetes.io/tls"
}
resource "kubernetes_deployment" "apps" {
  count = "${length(var.applications)}"

  metadata {
    name = "${lookup(var.applications[count.index], "name")}"

    labels = {
      application = "${lookup(var.applications[count.index], "name")}"
      created_by  = "terraform"
    }
  }

  spec {
    replicas = "${lookup(var.applications[count.index], "min_replicas")}"

    selector {
      match_labels = {
        application = "${lookup(var.applications[count.index], "name")}"
      }
    }

    template {
      metadata {
        labels = {
          application = "${lookup(var.applications[count.index], "name")}"
        }
      }

      spec {
        container {
          image             = "${lookup(var.applications[count.index], "image")}"
          image_pull_policy = "Always"
          name              = "${lookup(var.applications[count.index], "name")}"
          env_from {
            config_map_ref {
              name = "${var.environment}-config-${random_id.cluster_id.hex}"
            }
          }
          env_from {
            secret_ref {
              name = "${var.environment}-secrets-${random_id.cluster_id.hex}"
            }
          }

          liveness_probe {
            http_get {
              path = "${lookup(var.applications[count.index], "probe_path")}"
              port = "${lookup(var.applications[count.index], "target_port")}"
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }
        container {
          image   = "gcr.io/cloudsql-docker/gce-proxy:1.15"
          name    = "cloudsql-proxy"
          command = ["/cloud_sql_proxy", "-instances=pdes-production:us-central1:gamma-pdes-m-bb1ddb83aef98af1-0-db=tcp:3306", "-credential_file=/app/config/credentials.json"]

          volume_mount {
            mount_path = "/app/config/"
            name       = "credentials"
          }
        }
        volume {
          name = "credentials"
          secret {
            secret_name  = "${var.environment}-secrets-${random_id.cluster_id.hex}"
            default_mode = "0420"
          }
        }
      }
    }
  }
}
resource "kubernetes_horizontal_pod_autoscaler" "autoscaler" {
  count = "${length(var.applications)}"

  metadata {
    name = "${lookup(var.applications[count.index], "name")}"
  }
  spec {
    max_replicas = "${lookup(var.applications[count.index], "max_replicas")}"
    min_replicas = "${lookup(var.applications[count.index], "min_replicas")}"
    scale_target_ref {
      api_version = "v3"
      kind        = "Deployment"
      name        = "${lookup(var.applications[count.index], "name")}"
    }
  }
}
resource "kubernetes_service" "services" {
  count = "${length(var.applications)}"

  metadata {
    name = "${var.environment}-${lookup(var.applications[count.index], "name")}-service-${random_id.cluster_id.hex}"

    labels = {
      application = "${lookup(var.applications[count.index], "name")}"
    }
  }

  spec {
    selector = {
      application = "${lookup(var.applications[count.index], "name")}"
    }

    port {
      port        = "${lookup(var.applications[count.index], "port")}"
      target_port = "${lookup(var.applications[count.index], "target_port")}"
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress" "ingress" {
  metadata {
    name = "${var.environment}-${var.platform}-ingress-${random_id.cluster_id.hex}"
  }

  spec {
    backend {
      service_name = "${var.environment}-${lookup(var.applications[0], "name")}-service-${random_id.cluster_id.hex}"
      service_port = "${lookup(var.applications[0], "port")}"
    }

    dynamic "rule" {
      for_each = [for app_ingress in var.applications : {
        service_name = "${var.environment}-${app_ingress.name}-service-${random_id.cluster_id.hex}"
        service_port = app_ingress.port
        ingress_path = app_ingress.ingress_path
        host         = app_ingress.ingress_host
      }]

      content {
        host = rule.value.host
        http {
          path {
            backend {
              service_name = rule.value.service_name
              service_port = rule.value.service_port
            }
            path = rule.value.ingress_path

          }
        }
      }
    }

    tls {
      hosts       = ["timeoff.mon5.com"]
      secret_name = "${var.tls}"
    }
  }
}