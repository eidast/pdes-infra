terraform {
  required_version = ">= 0.12"
}
resource "random_id" "server" {
  byte_length = 8
}

resource "random_id" "username" {
  byte_length = 2
}

resource "random_id" "password" {
  byte_length = 8
}

resource "google_sql_database_instance" "master" {
  name             = "${var.environment}-${var.platform}-m-${lower(random_id.server.hex)}-0-db"
  database_version = var.database_version
  region           = var.main_region

  settings {
    tier              = var.tier
    activation_policy = var.activation_policy
    disk_size         = var.disk_size
    disk_type         = var.disk_type
    disk_autoresize   = var.disk_autoresize
    backup_configuration {
            enabled            = true
            binary_log_enabled = true
            start_time         = "00:00"
        }

    replication_type = var.replication_type

    maintenance_window {
        day          = 1        # Day of week (1-7), starting on Monday
        hour         = 3        # Hour of day (0-23), ignored if day not set
        update_track = "stable" # Receive updates earlier (canary) or later (stable)
    }
    # ip_configuration {
    #   private_network = var.private_network
    # }

    user_labels = {
      built-by    = "terraform"
      module      = "sql"
      environment = var.environment
    }
  }
}

resource "google_sql_database_instance" "read_replica" {
  count = var.read_replica

  name                 = "${var.environment}-${var.platform}-rr-${lower(random_id.server.hex)}-${count.index}-db"
  database_version     = var.database_version
  region               = var.main_region
  master_instance_name = google_sql_database_instance.master.name

  settings {
    tier = var.tier

    user_labels = {
      built-by    = "terraform"
      module      = "sql"
      master      = google_sql_database_instance.master.name
      environment = var.environment
    }
  }

  depends_on = [google_sql_database_instance.master]
}

resource "google_sql_database_instance" "failover_replica" {
  count = var.failover_replica

  name                 = "${var.environment}-${var.platform}-fo-${lower(random_id.server.hex)}1-${count.index}-db"
  database_version     = var.database_version
  region               = var.main_region
  master_instance_name = google_sql_database_instance.master.name

  replica_configuration {
    failover_target = true
  }

  settings {
    tier              = var.tier
    activation_policy = var.activation_policy
    disk_size         = var.disk_size
    disk_type         = var.disk_type
    disk_autoresize   = var.disk_autoresize
    # dynamic "ip_configuration" {
    #   for_each = [var.ip_configuration]
    #   content {
    #     # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
    #     # which keys might be set in maps assigned here, so it has
    #     # produced a comprehensive set here. Consider simplifying
    #     # this after confirming which keys can be set in practice.

    #     ipv4_enabled    = lookup(ip_configuration.value, "ipv4_enabled", null)
    #     private_network = lookup(ip_configuration.value, "private_network", null)
    #     require_ssl     = lookup(ip_configuration.value, "require_ssl", null)

    #     dynamic "authorized_networks" {
    #       for_each = lookup(ip_configuration.value, "authorized_networks", [])
    #       content {
    #         expiration_time = lookup(authorized_networks.value, "expiration_time", null)
    #         name            = lookup(authorized_networks.value, "name", null)
    #         value           = lookup(authorized_networks.value, "value", null)
    #       }
    #     }
    #   }
    # }
    replication_type       = var.replication_type
    availability_type      = "ZONAL"
    crash_safe_replication = true

    user_labels = {
      built-by    = "terraform"
      module      = "sql"
      master      = google_sql_database_instance.master.name
      environment = var.environment
    }
  }

  depends_on = [
    google_sql_database_instance.master,
    google_sql_database_instance.read_replica,
  ]
}

resource "google_sql_user" "admin" {
  name     = "admin-${random_id.username.hex}"
  instance = google_sql_database_instance.master.name
  host     = "%"
  password = random_id.password.hex

  depends_on = [google_sql_database_instance.master]
}

resource "google_sql_database" "database" {
  name = "${var.database_name}"
  instance = "${google_sql_database_instance.master.name}"
}