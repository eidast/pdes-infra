variable "environment" {
  description = "Define which enviroment is deployed"
}

variable "main_region" {
  description = "Region to be used in the master Google SQL resources"
}

variable "database_version" {
  description = "Kind of database to be executed"
  default     = "MYSQL_5_7"
}

variable "tier" {
  description = "The machine tier. supported tiers and pricing: https://cloud.google.com/sql/pricing"
  default     = "db-g1-micro"
}

variable "activation_policy" {
  description = "This specifies when the instance should be active. Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  default     = "ALWAYS"
}

variable "replication_type" {
  description = "Replication type for this instance, can be one of `ASYNCHRONOUS` or `SYNCHRONOUS`."
  default     = "SYNCHRONOUS"
}

variable "disk_type" {
  description = "Second generation only. The type of data disk: `PD_SSD` or `PD_HDD`."
  default     = "PD_SSD"
}

variable "disk_size" {
  description = "Second generation only. The size of data disk, in GB. Size of a running instance cannot be reduced but can be increased."
  default     = 10
}

variable "disk_autoresize" {
  description = "Second Generation only. Configuration to increase storage size automatically."
  default     = true
}

variable "ip_configuration" {
  description = "The ip_configuration settings subblock"
  type        = list(string)
  default     = []
}

variable "backup_configuration" {
  description = "The backup_configuration settings subblock for the database setings"
  type        = map(string)
  default     = {}
}

variable "read_replica" {
  description = "Quantity of read replica nodes for Google SQL"
  default     = 0
}

variable "failover_replica" {
  description = "Quantity of failover nodes for Google SQL"
  default     = 0
}

variable "maintenance_window" {
  description = "Values to define Maintenance window"
  type        = map(any)
}

# variable "private_network" {
#   description = "Private Network Name"
# }

variable "platform" {
  description = "Platform or component to deploy"
}

variable "database_name" {
  description = "Name of the database schema to create by default"
}
