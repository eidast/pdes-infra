output "admin_username" {
  value = google_sql_user.admin.name
}

output "admin_password" {
  value = google_sql_user.admin.password
}

