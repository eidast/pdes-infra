variable "db_tier" {
  description = "Kind of machine type for Google SQL"
  default     = "db-f1-micro"
}

variable "db_backup_configuration" {
  description = "Backup configuration"
  type        = map(any)
  default = {
    enabled            = true
    binary_log_enabled = true
    start_time         = "00:00"
  }
}

variable "db_maintenance_window" {
  description = "Define Maintenance Window"
  type        = map(any)
  default = {
    day          = 1        # Day of week (1-7), starting on Monday
    hour         = 3        # Hour of day (0-23), ignored if day not set
    update_track = "stable" # Receive updates earlier (canary) or later (stable)
  }
}
variable "database_name" {
  default = "db_timeoff"
}


