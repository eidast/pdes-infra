# Production
terraform {
  required_version = ">= 0.12"
}
provider "google" {
  credentials = file(
    "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json",
  )
  project = data.terraform_remote_state.infra-base.outputs.project
  region  = data.terraform_remote_state.infra-base.outputs.main_region
}

provider "google-beta" {
  credentials = file(
    "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json",
  )
  project = data.terraform_remote_state.infra-base.outputs.project
  region  = data.terraform_remote_state.infra-base.outputs.main_region
}

terraform {
  backend "gcs" {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-sql/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

data "terraform_remote_state" "infra-base" {
  backend = "gcs"

  config = {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-base/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

module "database" {
  source = "../../modules/resource/sql"

  tier                 = var.db_tier
  environment          = data.terraform_remote_state.infra-base.outputs.environment
  platform             = data.terraform_remote_state.infra-base.outputs.platform
  main_region          = data.terraform_remote_state.infra-base.outputs.main_region
  backup_configuration = var.db_backup_configuration
  maintenance_window   = var.db_maintenance_window

  failover_replica = 1
  read_replica     = 0
  database_name = var.database_name

  #private_network = "projects/${data.terraform_remote_state.infra-base.outputs.project}/global/networks/${data.terraform_remote_state.infra-base.outputs.main-network_name}"
}

