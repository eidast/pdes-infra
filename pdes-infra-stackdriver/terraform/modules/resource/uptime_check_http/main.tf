resource "google_monitoring_uptime_check_config" "http" {
  count             = "${length(var.checks)}"
  display_name      = "${lookup(var.checks[count.index], "display_name")}"
  timeout           = "${lookup(var.checks[count.index], "timeout", "10s")}"
  period            = "${lookup(var.checks[count.index], "period", "300s")}"
  selected_regions  = "${split(",", lookup(var.checks[count.index], "regions", "USA,EUROPE,SOUTH_AMERICA,ASIA_PACIFIC"))}"

  http_check {
    path    = "${lookup(var.checks[count.index], "path", "/")}"
    port    = "${lookup(var.checks[count.index], "port", "443")}"
    use_ssl = "${lookup(var.checks[count.index], "use_ssl", "true")}"
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id    = "${var.project}"
      host          = "${lookup(var.checks[count.index], "host")}"
    }
  }

  content_matchers {
    content = "${lookup(var.checks[count.index], "content", "")}"
  }
}

#resource "google_monitoring_alert_policy" "policy" {
#  count             = "${length(var.checks)}"
#  display_name  = "${lookup(var.checks[count.index], "display_name")}"
#  combiner      = "OR"
#  conditions {
#    display_name  = "${lookup(var.checks[count.index], "display_name")}"
#    condition_threshold {
#      filter    = "metric.type=\"monitoring.googleapis.com/uptime_check/check_passed\" AND metric.label.check_id=\"{google_monitoring_uptime_check_config.http[count.index].uptime_check_id}\" AND resource.type=\"uptime_url\""
#      duration  = "60s"
#      comparison = "COMPARISON_GT"
#      threshold_value = 1
#      trigger {
#          count = 1
#      }
#    } 
#  }
#  documentation {
#    content = "This site looks like down."
#  }
#  notification_channels = [
#  ]
#}
