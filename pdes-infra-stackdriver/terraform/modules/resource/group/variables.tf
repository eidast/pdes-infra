variable "groups" {
  type        = "list"
  description = "Expresion to find to create a stackdriver group"
}

variable "names" {
  type        = "list"
  description = "Name to define each stackdriver group"
}