# Monitoring groups
resource "google_monitoring_group" "stackdriver_group" {
  count        = "${length(var.groups)}"
  display_name = "${element(var.names, count.index)}"
  filter       = "resource.metadata.name=has_substring(\"${element(var.groups, count.index)}\")"
}