resource "google_monitoring_notification_channel" "stackdriver_phone_notification_channel" {
    count = "${length(var.names)}"
    display_name = "${element(var.names, count.index)}'s Phone"
    type = "sms"
    labels = {
        number = "${element(var.phones, count.index)}"
    }
}

resource "google_monitoring_notification_channel" "stackdriver_email_notification_channel" {
    count = "${length(var.names)}"
    display_name = "${element(var.names, count.index)}'s Email"
    type = "email"
    labels = {
        email_address = "${element(var.emails, count.index)}"
    }
}