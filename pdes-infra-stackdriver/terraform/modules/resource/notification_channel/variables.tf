variable "names" {
    type        = "list"
    description = "List of names for support personal"
}
variable "emails" {
    type        = "list"
    description = "List of emails for support personal"
}

variable "phones" {
    type        = "list"
    description = "Cellphone's list of the support personal"
}