resource "google_monitoring_uptime_check_config" "tcp" {
  count             = "${length(var.checks)}"
  display_name      = "${lookup(var.checks[count.index], "display_name")}"
  timeout           = "${lookup(var.checks[count.index], "timeout", "10s")}"
  period            = "${lookup(var.checks[count.index], "period", "300s")}"
  selected_regions  = "${split(",", lookup(var.checks[count.index], "regions", "USA,EUROPE,SOUTH_AMERICA,ASIA_PACIFIC"))}"

  tcp_check {
    port    = "${lookup(var.checks[count.index], "port")}"
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id    = "${var.project}"
      host          = "${lookup(var.checks[count.index], "host")}"
    }
  }
}