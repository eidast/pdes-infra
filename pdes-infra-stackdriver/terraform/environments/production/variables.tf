variable "https_checks" {
    description = ""
    default     = [
        {
            display_name    = "timeoff.mon5.com - Login Page"
            host            = "timeoff.mon5.com"
            path            = "/login"
            period          = "60s"
        },
    ]
}

variable "tcp_checks" {
    description = ""
    default     = [
        # {
        #     display_name    = "btc.domain.com"
        #     host            = "btc.domain.com"
        #     period          = "60s"
        #     port            = 50001
        # },
    ]
}