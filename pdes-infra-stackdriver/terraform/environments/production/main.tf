provider "google" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${data.terraform_remote_state.infra-base.outputs.project}"
  region      = "${data.terraform_remote_state.infra-base.outputs.main_region}"
}

provider "google-beta" {
  credentials = "${file("../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json")}"
  project     = "${data.terraform_remote_state.infra-base.outputs.project}"
  region      = "${data.terraform_remote_state.infra-base.outputs.main_region}"
}

terraform {
  backend "gcs" {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/prod/infra-stackdriver/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

data "terraform_remote_state" "infra-base" {
  backend = "gcs"

  config = {
    bucket      = "tf-pdes-state-production"
    prefix      = "tf/pdes/production/infra-base/"
    credentials = "../../../../../../gl-secrets/pdes-production-7cc942fa4db1.json"
  }
}

module "stackdriver_groups" {
  source            = "../../modules/resource/group"
  groups            = ["gke-", "-ba-", "-je-"]
  names             = ["GKE", "Bastion", "Jenkins"]
}
module "stackdriver_notification_channels" {
  source            = "../../modules/resource/notification_channel"
  names             = [ "Alexander Moreno Delgado"
                      ]
  phones            = [ "+50683952624", 
                      ]
  emails            = [ "eidast@gmail.com", 
                      ]
}

module "stackdriver_http_uptime_checks" {
  source = "../../modules/resource/uptime_check_http"
  
  checks  = "${var.https_checks}"
  project = "${data.terraform_remote_state.infra-base.outputs.project}"
}

module "stackdriver_tcp_uptime_checks" {
  source = "../../modules/resource/uptime_check_tcp"
  
  checks  = "${var.tcp_checks}"
  project = "${data.terraform_remote_state.infra-base.outputs.project}"
}