# Gorilla Logic -  Project

- Author: Alexander Moreno Delgado
- Email: eidast@gmail.com

## Objective:

As DevOps engineer, I have to deploy a timeoff-management (https://bitbucket.org/eidast/pdes-application/src/master/) application running on some cloud provider, automatically deployed through some sort of automation.

## Design

A technological solution will be designed to meet the requirements exposed previously, additionally adding a cost-efficient approach.

My scenario is based on Google Cloud Platform, using as many managed services as possible, as well as the use of other providers such as Cloudflare to provide some additional security features.

The entire solution will be mostly defined in IaC code to provide elasticity and immutability to our design.

Application lifecycle management will be managed using a CI/CD system in this case Jenkins was the chosen to lead these tasks, this builds our artifacts and those are sent to our private registry in GCR.io, all our workers are created on-demand using preemption.

My planning was handled 100% with text files using markdown, my main reason this project is managed only by me, however, in team projects this approach is not adequate.

The application will be deployed into a Kubernetes cluster, our cluster is using preemptible instances, so each node in our node pool is recreated automatically at least every 24 hours. 

A global load balancer that will provide access globally and will pass through Cloudflare to protect from DDoS attacks and provide SSL external layer. 

From the database perspective, I'm providing a high availability solution, if our master instance fails and this didn't restore service in 60 seconds a switch process start to our failover replica.

In general use preemptible instances allow us, save up to 80% in cost.

## Diagram

![Technical Diagram](diagram.jpg)

## Additional material

- [DevOps Presentation](https://docs.google.com/presentation/d/1obiA6GO6CzHX_oxsB-NAtNijfqPe8S3ICTRdWsKzJBo/edit?usp=sharing)
- [My personal log](captain_log.md)
- [Cloudflare integration with GCP](https://www.cloudflare.com/integrations/google-cloud/)

## Toolchain

- Google Cloud
- Packer
- Ansible
- Jenkins
- Bitbucket
- Terraform
- Kubernetes
- MySQL

## Terraform modules

|Name|Description|
|---|---|
|pdes-infra-base|Infrastructure base, networking, firewall, etc.|
|pdes-infra-gke|Google Kubernetes Engine|
|pdes-infra-sql|Google SQL (MySQL)|
|pdes-infra-stackdriver|Google SQL (MySQL)|

## Packer images

These images could be found in pdes-packer-images in src folder

|Name|Description|
|---|---|
|bastion-development.json|Bastion image for development project|
|bastion-production.json|Bastion image for production environment|
|jenkins-development.json|Jenkins MASTER image for development project|
|jenkins-production.json|Jenkins MASTER image for production project|
|jenkins-worker-production.json|Jenkins WORKER image for production project|

## Ansible Playbooks

|Name|Description|
|---|---|
|bastion-[ENVIROMENT].yaml|Initial configuration for our bastion server|
|jenkins-[ENVIRONMENT].yaml|Initial configuration for our master Jenkins|
|jenkins-worker-[ENVIRONMENT]|Configuration for our on-demand workers|

## Applications

|Name|Repo|Description|
|---|---|---|
|pdes-application|git clone git@bitbucket.org:eidast/pdes-application.git|Web application for managing employee absences.|

### How to add a new application

The process to add a new application is very simple, the module at charge is **infra-pdes-gke** in `variables.tf`

In our variable name **deploy_apps** add a new item following this convention.

```json
    {
      name         = "{app name}"
      image        = "{docker image}"
      min_replicas = 5
      max_replicas = 10
      port         = 80
      target_port  = 3000
      ingress_path = "/"
      ingress_host = "{domain name}"
      probe_path   = "{page to get 200 status}"
    }
```

## Monitoring

### How to add a new uptime check

The module which manage this is **infra-pdes-stackdriver** in `variables.tf`

At this moment only support 2 methods:
- http(s) checks
- tcp checks 

#### For TCP checks:

Add a new item in tcp_checks variable:

```json
     {
         display_name    = "btc.domain.com"
         host            = "btc.domain.com"
         period          = "60s"
         port            = 50001
     }
```

#### For HTTP:

```json
        {
            display_name    = "websitewithoutssl.com"
            use_ssl         = false
            port            = 80
            host            = "websitewithoutssl.com"
        }
```

#### For HTTPS:

```json
    {
        display_name    = "website.com"
        host            = "website.com"
        content         = "Any content I want to check here"
        period          = "60s"
        regions         = "EUROPE,SOUTH_AMERICA,ASIA_PACIFIC"
    }
```

## Security

### Cloudflare SSL policies

- Minimal version for TLS 1.2
- TLS 1.3 Supported
- Only use https
- Rewrite HTTP to HTTPS

### Database perspective:

Cloud SQL Proxy works by having a local client, called the proxy, running in the local environment. Your application communicates with the proxy with the standard database protocol used by your database. The proxy uses a secure tunnel to communicate with its companion process running on the server.

### Secret management

Every secret was managed only in my local machine at this moment, sharing the structure.

![Technical Diagram](secrets.png)

## Websites

- Timeoff app: https://timeoff.mon5.com
- Jenkins: https://gjenkins.mon5.com:8443/

## Usual Commands

### k8s 

#### Authenticate from the bastion

- `gcloud config set project [PROJECT]`
- `gcloud config set compute/zone [REGION]`
- `gcloud container clusters get-credentials [CLUSTER_NAME]`
